// $ANTLR 3.5.1 /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2020-04-01 03:54:54

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ExprParser extends DebugParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "BEGIN", "DIV", "END", "ID", "INT", 
		"LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "RP", "VAR", "WS"
	};
	public static final int EOF=-1;
	public static final int BEGIN=4;
	public static final int DIV=5;
	public static final int END=6;
	public static final int ID=7;
	public static final int INT=8;
	public static final int LP=9;
	public static final int MINUS=10;
	public static final int MUL=11;
	public static final int NL=12;
	public static final int PLUS=13;
	public static final int PODST=14;
	public static final int RP=15;
	public static final int VAR=16;
	public static final int WS=17;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public static final String[] ruleNames = new String[] {
		"invalidRule", "stat", "multExpr", "blok", "prog", "expr", "atom"
	};

	public static final boolean[] decisionCanBacktrack = new boolean[] {
		false, // invalid decision
		false, false, false, false, false, false, false
	};

 
	public int ruleLevel = 0;
	public int getRuleLevel() { return ruleLevel; }
	public void incRuleLevel() { ruleLevel++; }
	public void decRuleLevel() { ruleLevel--; }
	public ExprParser(TokenStream input) {
		this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
	}
	public ExprParser(TokenStream input, int port, RecognizerSharedState state) {
		super(input, state);
		DebugEventSocketProxy proxy =
			new DebugEventSocketProxy(this,port,adaptor);
		setDebugListener(proxy);
		setTokenStream(new DebugTokenStream(input,proxy));
		try {
			proxy.handshake();
		}
		catch (IOException ioe) {
			reportError(ioe);
		}
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);
		proxy.setTreeAdaptor(adap);
	}

	public ExprParser(TokenStream input, DebugEventListener dbg) {
		super(input, dbg);
		 
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);

	}

	protected boolean evalPredicate(boolean result, String predicate) {
		dbg.semanticPredicate(result, predicate);
		return result;
	}

		protected DebugTreeAdaptor adaptor;
		public void setTreeAdaptor(TreeAdaptor adaptor) {
			this.adaptor = new DebugTreeAdaptor(dbg,adaptor);
		}
		public TreeAdaptor getTreeAdaptor() {
			return adaptor;
		}
	@Override public String[] getTokenNames() { return ExprParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }


	public static class prog_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "prog"
	// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat | blok )+ EOF !;
	public final ExprParser.prog_return prog() throws RecognitionException {
		ExprParser.prog_return retval = new ExprParser.prog_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EOF3=null;
		ParserRuleReturnScope stat1 =null;
		ParserRuleReturnScope blok2 =null;

		CommonTree EOF3_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "prog");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(16, 0);

		try {
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat | blok )+ EOF !)
			dbg.enterAlt(1);

			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat | blok )+ EOF !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(17,7);
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat | blok )+
			int cnt1=0;
			try { dbg.enterSubRule(1);

			loop1:
			while (true) {
				int alt1=3;
				try { dbg.enterDecision(1, decisionCanBacktrack[1]);

				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==VAR) ) {
					alt1=1;
				}
				else if ( (LA1_0==BEGIN) ) {
					alt1=2;
				}

				} finally {dbg.exitDecision(1);}

				switch (alt1) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
					{
					dbg.location(17,8);
					pushFollow(FOLLOW_stat_in_prog49);
					stat1=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat1.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:15: blok
					{
					dbg.location(17,15);
					pushFollow(FOLLOW_blok_in_prog53);
					blok2=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok2.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt1++;
			}
			} finally {dbg.exitSubRule(1);}
			dbg.location(17,25);
			EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_prog57); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(17, 25);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "prog");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "prog"


	public static class blok_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "blok"
	// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:1: blok : BEGIN ^ ( stat | blok )* END !;
	public final ExprParser.blok_return blok() throws RecognitionException {
		ExprParser.blok_return retval = new ExprParser.blok_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token BEGIN4=null;
		Token END7=null;
		ParserRuleReturnScope stat5 =null;
		ParserRuleReturnScope blok6 =null;

		CommonTree BEGIN4_tree=null;
		CommonTree END7_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "blok");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(19, 0);

		try {
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:6: ( BEGIN ^ ( stat | blok )* END !)
			dbg.enterAlt(1);

			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:8: BEGIN ^ ( stat | blok )* END !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(19,13);
			BEGIN4=(Token)match(input,BEGIN,FOLLOW_BEGIN_in_blok66); 
			BEGIN4_tree = (CommonTree)adaptor.create(BEGIN4);
			root_0 = (CommonTree)adaptor.becomeRoot(BEGIN4_tree, root_0);
			dbg.location(19,15);
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:15: ( stat | blok )*
			try { dbg.enterSubRule(2);

			loop2:
			while (true) {
				int alt2=3;
				try { dbg.enterDecision(2, decisionCanBacktrack[2]);

				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= ID && LA2_0 <= LP)||LA2_0==NL||LA2_0==VAR) ) {
					alt2=1;
				}
				else if ( (LA2_0==BEGIN) ) {
					alt2=2;
				}

				} finally {dbg.exitDecision(2);}

				switch (alt2) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:16: stat
					{
					dbg.location(19,16);
					pushFollow(FOLLOW_stat_in_blok70);
					stat5=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat5.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:23: blok
					{
					dbg.location(19,23);
					pushFollow(FOLLOW_blok_in_blok74);
					blok6=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok6.getTree());

					}
					break;

				default :
					break loop2;
				}
			}
			} finally {dbg.exitSubRule(2);}
			dbg.location(19,33);
			END7=(Token)match(input,END,FOLLOW_END_in_blok78); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(20, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blok");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "blok"


	public static class stat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "stat"
	// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:22:1: stat : ( expr NL -> expr | VAR ID ( PODST expr )? NL -> ^( VAR ID ) ( ^( PODST ID expr ) )? | ID PODST expr NL -> ^( PODST ID expr ) | NL ->);
	public final ExprParser.stat_return stat() throws RecognitionException {
		ExprParser.stat_return retval = new ExprParser.stat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token NL9=null;
		Token VAR10=null;
		Token ID11=null;
		Token PODST12=null;
		Token NL14=null;
		Token ID15=null;
		Token PODST16=null;
		Token NL18=null;
		Token NL19=null;
		ParserRuleReturnScope expr8 =null;
		ParserRuleReturnScope expr13 =null;
		ParserRuleReturnScope expr17 =null;

		CommonTree NL9_tree=null;
		CommonTree VAR10_tree=null;
		CommonTree ID11_tree=null;
		CommonTree PODST12_tree=null;
		CommonTree NL14_tree=null;
		CommonTree ID15_tree=null;
		CommonTree PODST16_tree=null;
		CommonTree NL18_tree=null;
		CommonTree NL19_tree=null;
		RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
		RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try { dbg.enterRule(getGrammarFileName(), "stat");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(22, 0);

		try {
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:5: ( expr NL -> expr | VAR ID ( PODST expr )? NL -> ^( VAR ID ) ( ^( PODST ID expr ) )? | ID PODST expr NL -> ^( PODST ID expr ) | NL ->)
			int alt4=4;
			try { dbg.enterDecision(4, decisionCanBacktrack[4]);

			switch ( input.LA(1) ) {
			case INT:
			case LP:
				{
				alt4=1;
				}
				break;
			case ID:
				{
				int LA4_2 = input.LA(2);
				if ( (LA4_2==PODST) ) {
					alt4=3;
				}
				else if ( (LA4_2==DIV||(LA4_2 >= MINUS && LA4_2 <= PLUS)) ) {
					alt4=1;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 4, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VAR:
				{
				alt4=2;
				}
				break;
			case NL:
				{
				alt4=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(4);}

			switch (alt4) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:7: expr NL
					{
					dbg.location(23,7);
					pushFollow(FOLLOW_expr_in_stat96);
					expr8=expr();
					state._fsp--;

					stream_expr.add(expr8.getTree());dbg.location(23,12);
					NL9=(Token)match(input,NL,FOLLOW_NL_in_stat98);  
					stream_NL.add(NL9);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 23:15: -> expr
					{
						dbg.location(23,18);
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:7: VAR ID ( PODST expr )? NL
					{
					dbg.location(25,7);
					VAR10=(Token)match(input,VAR,FOLLOW_VAR_in_stat111);  
					stream_VAR.add(VAR10);
					dbg.location(25,11);
					ID11=(Token)match(input,ID,FOLLOW_ID_in_stat113);  
					stream_ID.add(ID11);
					dbg.location(25,14);
					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:14: ( PODST expr )?
					int alt3=2;
					try { dbg.enterSubRule(3);
					try { dbg.enterDecision(3, decisionCanBacktrack[3]);

					int LA3_0 = input.LA(1);
					if ( (LA3_0==PODST) ) {
						alt3=1;
					}
					} finally {dbg.exitDecision(3);}

					switch (alt3) {
						case 1 :
							dbg.enterAlt(1);

							// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:15: PODST expr
							{
							dbg.location(25,15);
							PODST12=(Token)match(input,PODST,FOLLOW_PODST_in_stat116);  
							stream_PODST.add(PODST12);
							dbg.location(25,21);
							pushFollow(FOLLOW_expr_in_stat118);
							expr13=expr();
							state._fsp--;

							stream_expr.add(expr13.getTree());
							}
							break;

					}
					} finally {dbg.exitSubRule(3);}
					dbg.location(25,28);
					NL14=(Token)match(input,NL,FOLLOW_NL_in_stat122);  
					stream_NL.add(NL14);

					// AST REWRITE
					// elements: VAR, PODST, ID, expr, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 25:31: -> ^( VAR ID ) ( ^( PODST ID expr ) )?
					{
						dbg.location(25,34);
						// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:34: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(25,36);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(25,40);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}
						dbg.location(25,44);
						// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:44: ( ^( PODST ID expr ) )?
						if ( stream_PODST.hasNext()||stream_ID.hasNext()||stream_expr.hasNext() ) {
							dbg.location(25,44);
							// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:44: ^( PODST ID expr )
							{
							CommonTree root_1 = (CommonTree)adaptor.nil();
							dbg.location(25,46);
							root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
							dbg.location(25,52);
							adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(25,55);
							adaptor.addChild(root_1, stream_expr.nextTree());
							adaptor.addChild(root_0, root_1);
							}

						}
						stream_PODST.reset();
						stream_ID.reset();
						stream_expr.reset();

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:7: ID PODST expr NL
					{
					dbg.location(27,7);
					ID15=(Token)match(input,ID,FOLLOW_ID_in_stat151);  
					stream_ID.add(ID15);
					dbg.location(27,10);
					PODST16=(Token)match(input,PODST,FOLLOW_PODST_in_stat153);  
					stream_PODST.add(PODST16);
					dbg.location(27,16);
					pushFollow(FOLLOW_expr_in_stat155);
					expr17=expr();
					state._fsp--;

					stream_expr.add(expr17.getTree());dbg.location(27,21);
					NL18=(Token)match(input,NL,FOLLOW_NL_in_stat157);  
					stream_NL.add(NL18);

					// AST REWRITE
					// elements: ID, expr, PODST
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 27:24: -> ^( PODST ID expr )
					{
						dbg.location(27,27);
						// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:27: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(27,29);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(27,35);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(27,38);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:28:7: NL
					{
					dbg.location(28,7);
					NL19=(Token)match(input,NL,FOLLOW_NL_in_stat175);  
					stream_NL.add(NL19);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 28:10: ->
					{
						dbg.location(29,5);
						root_0 = null;
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(29, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "stat");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "stat"


	public static class expr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:31:1: expr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
	public final ExprParser.expr_return expr() throws RecognitionException {
		ExprParser.expr_return retval = new ExprParser.expr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUS21=null;
		Token MINUS23=null;
		ParserRuleReturnScope multExpr20 =null;
		ParserRuleReturnScope multExpr22 =null;
		ParserRuleReturnScope multExpr24 =null;

		CommonTree PLUS21_tree=null;
		CommonTree MINUS23_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "expr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(31, 0);

		try {
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
			dbg.enterAlt(1);

			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(32,7);
			pushFollow(FOLLOW_multExpr_in_expr194);
			multExpr20=multExpr();
			state._fsp--;

			adaptor.addChild(root_0, multExpr20.getTree());
			dbg.location(33,7);
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
			try { dbg.enterSubRule(5);

			loop5:
			while (true) {
				int alt5=3;
				try { dbg.enterDecision(5, decisionCanBacktrack[5]);

				int LA5_0 = input.LA(1);
				if ( (LA5_0==PLUS) ) {
					alt5=1;
				}
				else if ( (LA5_0==MINUS) ) {
					alt5=2;
				}

				} finally {dbg.exitDecision(5);}

				switch (alt5) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:9: PLUS ^ multExpr
					{
					dbg.location(33,13);
					PLUS21=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr204); 
					PLUS21_tree = (CommonTree)adaptor.create(PLUS21);
					root_0 = (CommonTree)adaptor.becomeRoot(PLUS21_tree, root_0);
					dbg.location(33,15);
					pushFollow(FOLLOW_multExpr_in_expr207);
					multExpr22=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr22.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:9: MINUS ^ multExpr
					{
					dbg.location(34,14);
					MINUS23=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr217); 
					MINUS23_tree = (CommonTree)adaptor.create(MINUS23);
					root_0 = (CommonTree)adaptor.becomeRoot(MINUS23_tree, root_0);
					dbg.location(34,16);
					pushFollow(FOLLOW_multExpr_in_expr220);
					multExpr24=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr24.getTree());

					}
					break;

				default :
					break loop5;
				}
			}
			} finally {dbg.exitSubRule(5);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(36, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "expr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "expr"


	public static class multExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multExpr"
	// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:1: multExpr : atom ( MUL ^ atom | DIV ^ atom )* ;
	public final ExprParser.multExpr_return multExpr() throws RecognitionException {
		ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MUL26=null;
		Token DIV28=null;
		ParserRuleReturnScope atom25 =null;
		ParserRuleReturnScope atom27 =null;
		ParserRuleReturnScope atom29 =null;

		CommonTree MUL26_tree=null;
		CommonTree DIV28_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "multExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(38, 0);

		try {
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:5: ( atom ( MUL ^ atom | DIV ^ atom )* )
			dbg.enterAlt(1);

			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:7: atom ( MUL ^ atom | DIV ^ atom )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(39,7);
			pushFollow(FOLLOW_atom_in_multExpr246);
			atom25=atom();
			state._fsp--;

			adaptor.addChild(root_0, atom25.getTree());
			dbg.location(40,7);
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:7: ( MUL ^ atom | DIV ^ atom )*
			try { dbg.enterSubRule(6);

			loop6:
			while (true) {
				int alt6=3;
				try { dbg.enterDecision(6, decisionCanBacktrack[6]);

				int LA6_0 = input.LA(1);
				if ( (LA6_0==MUL) ) {
					alt6=1;
				}
				else if ( (LA6_0==DIV) ) {
					alt6=2;
				}

				} finally {dbg.exitDecision(6);}

				switch (alt6) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:9: MUL ^ atom
					{
					dbg.location(40,12);
					MUL26=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr256); 
					MUL26_tree = (CommonTree)adaptor.create(MUL26);
					root_0 = (CommonTree)adaptor.becomeRoot(MUL26_tree, root_0);
					dbg.location(40,14);
					pushFollow(FOLLOW_atom_in_multExpr259);
					atom27=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom27.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:41:9: DIV ^ atom
					{
					dbg.location(41,12);
					DIV28=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr269); 
					DIV28_tree = (CommonTree)adaptor.create(DIV28);
					root_0 = (CommonTree)adaptor.becomeRoot(DIV28_tree, root_0);
					dbg.location(41,14);
					pushFollow(FOLLOW_atom_in_multExpr272);
					atom29=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom29.getTree());

					}
					break;

				default :
					break loop6;
				}
			}
			} finally {dbg.exitSubRule(6);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(43, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "multExpr"


	public static class atom_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:1: atom : ( INT | ID | LP ! expr RP !);
	public final ExprParser.atom_return atom() throws RecognitionException {
		ExprParser.atom_return retval = new ExprParser.atom_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token INT30=null;
		Token ID31=null;
		Token LP32=null;
		Token RP34=null;
		ParserRuleReturnScope expr33 =null;

		CommonTree INT30_tree=null;
		CommonTree ID31_tree=null;
		CommonTree LP32_tree=null;
		CommonTree RP34_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "atom");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(45, 0);

		try {
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:5: ( INT | ID | LP ! expr RP !)
			int alt7=3;
			try { dbg.enterDecision(7, decisionCanBacktrack[7]);

			switch ( input.LA(1) ) {
			case INT:
				{
				alt7=1;
				}
				break;
			case ID:
				{
				alt7=2;
				}
				break;
			case LP:
				{
				alt7=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(7);}

			switch (alt7) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:7: INT
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(46,7);
					INT30=(Token)match(input,INT,FOLLOW_INT_in_atom298); 
					INT30_tree = (CommonTree)adaptor.create(INT30);
					adaptor.addChild(root_0, INT30_tree);

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:47:7: ID
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(47,7);
					ID31=(Token)match(input,ID,FOLLOW_ID_in_atom306); 
					ID31_tree = (CommonTree)adaptor.create(ID31);
					adaptor.addChild(root_0, ID31_tree);

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:48:7: LP ! expr RP !
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(48,9);
					LP32=(Token)match(input,LP,FOLLOW_LP_in_atom314); dbg.location(48,11);
					pushFollow(FOLLOW_expr_in_atom317);
					expr33=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr33.getTree());
					dbg.location(48,18);
					RP34=(Token)match(input,RP,FOLLOW_RP_in_atom319); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(49, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "atom");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "atom"

	// Delegated rules



	public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x0000000000011390L});
	public static final BitSet FOLLOW_blok_in_prog53 = new BitSet(new long[]{0x0000000000011390L});
	public static final BitSet FOLLOW_EOF_in_prog57 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BEGIN_in_blok66 = new BitSet(new long[]{0x00000000000113D0L});
	public static final BitSet FOLLOW_stat_in_blok70 = new BitSet(new long[]{0x00000000000113D0L});
	public static final BitSet FOLLOW_blok_in_blok74 = new BitSet(new long[]{0x00000000000113D0L});
	public static final BitSet FOLLOW_END_in_blok78 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_stat96 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_NL_in_stat98 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat111 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_ID_in_stat113 = new BitSet(new long[]{0x0000000000005000L});
	public static final BitSet FOLLOW_PODST_in_stat116 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_expr_in_stat118 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_NL_in_stat122 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_stat151 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_PODST_in_stat153 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_expr_in_stat155 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_NL_in_stat157 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NL_in_stat175 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multExpr_in_expr194 = new BitSet(new long[]{0x0000000000002402L});
	public static final BitSet FOLLOW_PLUS_in_expr204 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_multExpr_in_expr207 = new BitSet(new long[]{0x0000000000002402L});
	public static final BitSet FOLLOW_MINUS_in_expr217 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_multExpr_in_expr220 = new BitSet(new long[]{0x0000000000002402L});
	public static final BitSet FOLLOW_atom_in_multExpr246 = new BitSet(new long[]{0x0000000000000822L});
	public static final BitSet FOLLOW_MUL_in_multExpr256 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_atom_in_multExpr259 = new BitSet(new long[]{0x0000000000000822L});
	public static final BitSet FOLLOW_DIV_in_multExpr269 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_atom_in_multExpr272 = new BitSet(new long[]{0x0000000000000822L});
	public static final BitSet FOLLOW_INT_in_atom298 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_atom306 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LP_in_atom314 = new BitSet(new long[]{0x0000000000000380L});
	public static final BitSet FOLLOW_expr_in_atom317 = new BitSet(new long[]{0x0000000000008000L});
	public static final BitSet FOLLOW_RP_in_atom319 = new BitSet(new long[]{0x0000000000000002L});
}
