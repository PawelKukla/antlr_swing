// $ANTLR 3.5.1 /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2020-04-01 03:54:55

package tb.antlr; 


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ExprLexer extends Lexer {
	public static final int EOF=-1;
	public static final int BEGIN=4;
	public static final int DIV=5;
	public static final int END=6;
	public static final int ID=7;
	public static final int INT=8;
	public static final int LP=9;
	public static final int MINUS=10;
	public static final int MUL=11;
	public static final int NL=12;
	public static final int PLUS=13;
	public static final int PODST=14;
	public static final int RP=15;
	public static final int VAR=16;
	public static final int WS=17;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ExprLexer() {} 
	public ExprLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ExprLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:51:5: ( 'var' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:51:6: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "BEGIN"
	public final void mBEGIN() throws RecognitionException {
		try {
			int _type = BEGIN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:53:7: ( '{' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:53:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BEGIN"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:55:5: ( '}' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:55:7: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:57:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:57:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:57:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:5: ( ( '0' .. '9' )+ )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:7: ( '0' .. '9' )+
			{
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			int _type = NL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:4: ( ( '\\r' )? '\\n' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:6: ( '\\r' )? '\\n'
			{
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:6: ( '\\r' )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='\r') ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:6: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:63:4: ( ( ' ' | '\\t' )+ )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:63:6: ( ' ' | '\\t' )+
			{
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:63:6: ( ' ' | '\\t' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0=='\t'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "LP"
	public final void mLP() throws RecognitionException {
		try {
			int _type = LP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:67:2: ( '(' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:67:4: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LP"

	// $ANTLR start "RP"
	public final void mRP() throws RecognitionException {
		try {
			int _type = RP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:71:2: ( ')' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:71:4: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RP"

	// $ANTLR start "PODST"
	public final void mPODST() throws RecognitionException {
		try {
			int _type = PODST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:75:2: ( '=' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:75:4: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PODST"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:79:2: ( '+' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:79:4: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:83:2: ( '-' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:83:4: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:87:2: ( '*' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:87:4: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:91:2: ( '/' )
			// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:91:4: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:8: ( VAR | BEGIN | END | ID | INT | NL | WS | LP | RP | PODST | PLUS | MINUS | MUL | DIV )
		int alt5=14;
		switch ( input.LA(1) ) {
		case 'v':
			{
			int LA5_1 = input.LA(2);
			if ( (LA5_1=='a') ) {
				int LA5_15 = input.LA(3);
				if ( (LA5_15=='r') ) {
					int LA5_16 = input.LA(4);
					if ( ((LA5_16 >= '0' && LA5_16 <= '9')||(LA5_16 >= 'A' && LA5_16 <= 'Z')||LA5_16=='_'||(LA5_16 >= 'a' && LA5_16 <= 'z')) ) {
						alt5=4;
					}

					else {
						alt5=1;
					}

				}

				else {
					alt5=4;
				}

			}

			else {
				alt5=4;
			}

			}
			break;
		case '{':
			{
			alt5=2;
			}
			break;
		case '}':
			{
			alt5=3;
			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case '_':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt5=4;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt5=5;
			}
			break;
		case '\n':
		case '\r':
			{
			alt5=6;
			}
			break;
		case '\t':
		case ' ':
			{
			alt5=7;
			}
			break;
		case '(':
			{
			alt5=8;
			}
			break;
		case ')':
			{
			alt5=9;
			}
			break;
		case '=':
			{
			alt5=10;
			}
			break;
		case '+':
			{
			alt5=11;
			}
			break;
		case '-':
			{
			alt5=12;
			}
			break;
		case '*':
			{
			alt5=13;
			}
			break;
		case '/':
			{
			alt5=14;
			}
			break;
		default:
			NoViableAltException nvae =
				new NoViableAltException("", 5, 0, input);
			throw nvae;
		}
		switch (alt5) {
			case 1 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:10: VAR
				{
				mVAR(); 

				}
				break;
			case 2 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:14: BEGIN
				{
				mBEGIN(); 

				}
				break;
			case 3 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:20: END
				{
				mEND(); 

				}
				break;
			case 4 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:24: ID
				{
				mID(); 

				}
				break;
			case 5 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:27: INT
				{
				mINT(); 

				}
				break;
			case 6 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:31: NL
				{
				mNL(); 

				}
				break;
			case 7 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:34: WS
				{
				mWS(); 

				}
				break;
			case 8 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:37: LP
				{
				mLP(); 

				}
				break;
			case 9 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:40: RP
				{
				mRP(); 

				}
				break;
			case 10 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:43: PODST
				{
				mPODST(); 

				}
				break;
			case 11 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:49: PLUS
				{
				mPLUS(); 

				}
				break;
			case 12 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:54: MINUS
				{
				mMINUS(); 

				}
				break;
			case 13 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:60: MUL
				{
				mMUL(); 

				}
				break;
			case 14 :
				// /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:64: DIV
				{
				mDIV(); 

				}
				break;

		}
	}



}
